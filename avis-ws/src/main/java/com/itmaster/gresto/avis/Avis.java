package com.itmaster.gresto.avis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Avis {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String pseudo;
    private String contenuAvis;

    //un  avis conserne un et un seul restaurant
    private long idRestaurant;

    public Avis(String pseudo, String contenuAvis, long idRestaurant) {
        this.pseudo = pseudo;
        this.contenuAvis = contenuAvis;
        this.idRestaurant = idRestaurant;
    }


    public Avis(String pseudo, String contenuAvis) {
        this.pseudo = pseudo;
        this.contenuAvis = contenuAvis;
    }
}
