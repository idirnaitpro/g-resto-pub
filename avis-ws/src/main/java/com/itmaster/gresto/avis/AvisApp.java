package com.itmaster.gresto.avis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.List;

@EnableDiscoveryClient
@SpringBootApplication
public class AvisApp {

    @Autowired
    private static AvisCrud crud;

    public static void main(String[] args) {
        SpringApplication.run(AvisApp.class, args);
    }




    @Bean
    public static List<Avis> setRestaurantApp(AvisCrud crud) {
        //Restaurant r = new Restaurant(null,"NAIT ALI", "Idir","","","",null);
        crud.save(new Avis("idir1","c'est toooop, je recommande.",1l));
        crud.save(new Avis("idir2","c'est toooop, je recommande.",1l));
        crud.save(new Avis("idir3","c'est toooop, je recommande.",1l));
        crud.save(new Avis("idir4","c'est toooop, je recommande.",2l));
        crud.save(new Avis("idir5","c'est toooop, je recommande.",3l));
        crud.save(new Avis("idir6","c'est toooop, je recommande.",4l));
        return (List<Avis>) crud.findAll();
    }
}
