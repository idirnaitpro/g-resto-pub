package com.itmaster.gresto.avis;

import org.springframework.data.repository.CrudRepository;

public interface AvisCrud extends CrudRepository<Avis,Long> {
}
