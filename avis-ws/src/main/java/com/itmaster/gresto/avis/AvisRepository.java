package com.itmaster.gresto.avis;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "avis", path = "avis")
public interface  AvisRepository extends PagingAndSortingRepository <Avis, Long> {

    List<Avis> findByIdRestaurant (Long idRestaurant);

}