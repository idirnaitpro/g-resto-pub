package com.itmaster.gresto.produit;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "produit", path = "produit")
public interface ProduitRepository extends PagingAndSortingRepository <Produit, Long> {

}