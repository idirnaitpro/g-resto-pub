package itmaster.gresto.restaurant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String nom;
    private String prenom;
    private String raisonSociale;
    private String numeroSiret;
    private String Adresse;

    //toute les info liés au logo sont retourné par le mic ser Logo
    private Long logo;

    //TODO : reflichir à commnent gerer le logo
    //


    public Restaurant(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }
}
