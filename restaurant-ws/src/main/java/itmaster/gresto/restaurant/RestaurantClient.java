package itmaster.gresto.restaurant;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;

public class RestaurantClient implements RestaurantRestClient{

    @Autowired
    RestaurantRestClient restaurantRestClient;

    @HystrixCommand (fallbackMethod = "defaultRestaurant")
    public Restaurant get(Long id){
        return  restaurantRestClient.get(id);
    }


    public Restaurant defaultRestaurant(Long id){
        return  new Restaurant(id + " not foud");
    }
}
