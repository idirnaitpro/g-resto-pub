package itmaster.gresto.restaurant;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ws.rs.PathParam;

@FeignClient("restaurant")
public interface RestaurantRestClient {

    @RequestMapping (method = RequestMethod.GET , value = "/restaurant/{id}")
    public Restaurant get(@PathParam("id") Long id);
}
