package itmaster.gresto.restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.util.List;

@EnableDiscoveryClient
//@EnableFeignClients(clients = {Restaurant.class})
@SpringBootApplication
public class RestaurantApp {

    @Autowired
    private static RestaurantCrud crud;

    public static void main(String[] args) {
        SpringApplication.run(RestaurantApp.class, args);
        //setRestaurantApp(crud);

    }



    @Bean
    public static List<Restaurant> setRestaurantApp(RestaurantCrud crud) {
        //Restaurant r = new Restaurant(null,"NAIT ALI", "Idir","","","",null);
        crud.save(new Restaurant(1l,"NAIT ALI", "Idir","LE-MED","3256958745","8 rue etienne dolet 95340",null));
        crud.save(new Restaurant(2l,"NAIT ALI", "Rabah","786","2548795632","12 rue de lille 94230",null));
        crud.save(new Restaurant(3l,"MADDI", "Massy","BONAPP","6254744478","55 place de france 92450",null));
        return (List<Restaurant>) crud.findAll();
    }


}
