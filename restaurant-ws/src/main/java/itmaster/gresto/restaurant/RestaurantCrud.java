package itmaster.gresto.restaurant;

import org.springframework.data.repository.CrudRepository;

public interface RestaurantCrud extends CrudRepository<Restaurant,Long> {
}